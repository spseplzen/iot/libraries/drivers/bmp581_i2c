/*
 * File:   bmp581.c
 * Author: Miroslav Soukup
 * Description: Source file of bmp581 pressure sensor driver.
 * 
 */



// *****************************************************************************
// Section: Included Files
// *****************************************************************************

#include <stdint.h> // data types
#include <stdlib.h>

#include "bmp581.h"



// *****************************************************************************
// Section: Functions
// *****************************************************************************

static uint8_t _bmp581_read_regs(bmp581_t *me, uint8_t reg, uint8_t *data, uint8_t size)
{
    if(!me) return 0;
    if(!me->i2c_write(me->client_address, &reg, 1)) return 0;
    if(!me->i2c_read(me->client_address, data, size)) return 0;
    return 1;
}

static uint8_t _bmp581_write_regs(bmp581_t *me, uint8_t reg, uint8_t *data, uint8_t size)
{
    if(!me) return 0;
    static uint8_t _data[32];
    _data[0] = reg;
    for(uint8_t i = 0; i < size; ++i) _data[i + 1] = data[i];
    if(!me->i2c_write(me->client_address, _data, size + 1)) return 0;
    return 1;
}

uint8_t bmp581_init (bmp581_t *me, uint16_t client_address, bmp581_config_t *config)
{
    if(!me) return 0;
    
    me->config = *config;
    me->client_address = client_address;
    
    uint8_t data[3];
    
    /* ************************ DEVICE ID ************************ */
    bmp581_get_device_id(me);
    if((me->data.device_id != BMP581_DEVICE_ID_PRIMARY) && (me->data.device_id != BMP581_DEVICE_ID_SECONDARY)) return 0;
    /* *********************************************************** */
    
    /* ********************** POWERUP CHECK ********************** */
    if(!_bmp581_read_regs(me, BMP581_REG_STATUS, data, 1)) return 0;
    if((!(data[0] & BMP581_INT_NVM_RDY)) || (data[0] & BMP581_INT_NVM_ERR)) return 0;
    /* *********************************************************** */

    /* *********************** INT STATUS ************************ */
    if(!_bmp581_read_regs(me, BMP581_REG_INT_STATUS, data, 1)) return 0;
    if(!(data[0] & 0b00010000)) return 0;
    /* *********************************************************** */

    /* **************** SET STANDBY MODE ************************* */
    if(!_bmp581_read_regs(me, BMP581_REG_ODR_CONFIG, data, 1)) return 0;

    if((data[1] & 0b11) != BMP581_POWER_MODE_STANDBY)
    {
        
        data[0] = data[0] & 0b01111111; // disable deep sleep
        data[0] = (data[0] & 0b11111100) | (BMP581_POWER_MODE_STANDBY & 0b11); // set power mode
        
        if(!_bmp581_write_regs(me, BMP581_REG_ODR_CONFIG, data, 1)) return 0;

        me->delay_us(BMP581_DELAY_US_STANDBY);
    }
    /* *********************************************************** */

    /* ***************** CHECK DEEP STANDBY MODE ***************** */
    if(!_bmp581_read_regs(me, BMP581_REG_FIFO_SEL, data, 1)) return 0;
    uint8_t power_mode = (data[0] & 0b11);

    // TO BE DONE
    
    if(power_mode != BMP581_POWER_MODE_STANDBY)
    {
        
    }
    /* *********************************************************** */

    /* ********************* OSR ODR CONFIG ********************** */
    if(!_bmp581_read_regs(me, BMP581_REG_OSR_CONFIG, data, 2)) return 0;

    data[0] = (data[0] & 0b11111000) | (me->config.osr_temperature_effective & 0b00000111);
    data[0] = (data[0] & 0b11000111) | ((me->config.osr_pressure_effective & 0b00000111) << 3);
    data[0] = (data[0] & 0b10111111) | ((me->config.pressure_enable & 0b00000001) << 6);
    data[1] = (data[1] & 0b10000011) | ((me->config.output_data_rate & 0b00011111) << 2);
    
    if(!_bmp581_write_regs(me, BMP581_REG_OSR_CONFIG, data, 2)) return 0;
    /* *********************************************************** */

    /* *********************** DSP CONFIG ************************ */
    if(!_bmp581_read_regs(me, BMP581_REG_DSP_CONFIG, data, 2)) return 0;

    data[0] = (data[0] & 0b11110111) | ((me->config.iir_shutdown_temperature & 0b00000001) << 3);
    data[0] = (data[0] & 0b11011111) | ((me->config.iir_shutdown_temperature & 0b00000001) << 5);
    data[0] = (data[0] & 0b11111011) | ((me->config.iir_flush_forced_enable & 0b00000001) << 2);
    //data[1] = (data[1] & 0b00000111) | (me->config.iir_filter_temperature & 0b00000111);
    data[1] = me->config.iir_filter_temperature;
    data[1] = (data[1] & 0b11000111) | ((me->config.iir_filter_pressure & 0b00000111) << 3);
    
    if(!_bmp581_write_regs(me, BMP581_REG_DSP_CONFIG, data, 2)) return 0;
    /* *********************************************************** */

    /* ******************** INTERRUPT CONFIG ********************* */
    if(!_bmp581_read_regs(me, BMP581_REG_INT_CONFIG, data, 2)) return 0;

    data[0] = (data[0] & 0b11111110) | (me->config.int_mode & 0b00000001);
    data[0] = (data[0] & 0b11111101) | ((me->config.int_polarity & 0b00000001) << 1);
    data[0] = (data[0] & 0b11111011) | ((me->config.int_drive & 0b00000001) << 2);
    data[0] = (data[0] & 0b11110111) | ((me->config.int_enable & 0b00000001) << 3);
    
    if(!_bmp581_write_regs(me, BMP581_REG_INT_CONFIG, data, 1)) return 0;
    /* *********************************************************** */

    /* ******************** INTERRUPT SOURCE ********************* */
    if(!_bmp581_read_regs(me, BMP581_REG_INT_SOURCE, data, 2)) return 0;

    data[0] = (data[0] & 0b11111110) | (me->config.drdy_enable & 0b00000001);
    data[0] = (data[0] & 0b11111101) | ((me->config.fifo_full_enable & 0b00000001) << 1);
    data[0] = (data[0] & 0b11111011) | ((me->config.fifo_thres_enable & 0b00000001) << 2);
    data[0] = (data[0] & 0b11110111) | ((me->config.oor_press_enable & 0b00000001) << 3);
    
    if(!_bmp581_write_regs(me, BMP581_REG_INT_SOURCE, data, 2)) return 0;
    /* *********************************************************** */

    /* **************** SET POWER MODE VIA CONFIG **************** */
    if(me->config.power_mode != BMP581_POWER_MODE_STANDBY)
    {
        if(!_bmp581_read_regs(me, BMP581_REG_ODR_CONFIG, data, 1)) return 0;

        data[0] =  data[0] & 0b01111111; // disable deep sleep
        data[0] = (data[0] & 0b11111100) | (me->config.power_mode & 0b11); // set power mode
        
        if(!_bmp581_write_regs(me, BMP581_REG_ODR_CONFIG, data, 1)) return 0;
        
        me->delay_us(BMP581_DELAY_US_STANDBY);
    }
    /* *********************************************************** */
    
    return 1;
}

uint8_t bmp581_get_data (bmp581_t *me)
{
    if(!me || me->config.power_mode == BMP581_POWER_MODE_STANDBY || me->config.power_mode == BMP581_POWER_MODE_DEEP_STANDBY) return 0;
    
    me->data.temperature = 0;
    me->data.pressure    = 0;
    
    uint8_t data[6];
    if(!_bmp581_read_regs(me, BMP581_REG_TEMP_DATA_XLSB, data, 6)) return 0;
    
    int32_t temperature_raw_data = (int32_t) ((int32_t) ((uint32_t)(((uint32_t)data[2] << 16) | ((uint16_t)data[1] << 8) | data[0]) << 8) >> 8);
    me->data.temperature = (float)(temperature_raw_data / 65536.0);
    
    if(me->config.pressure_enable == 1)
    {
        uint32_t pressure_raw_data = (uint32_t)((uint32_t)(data[5] << 16) | (uint16_t)(data[4] << 8) | data[3]);
        me->data.pressure = (float)(pressure_raw_data / 64.0);
    }
    else
    {
        me->data.pressure = 0.0;
    }
    
    return 1;
}

void bmp581_convert_data_to_absolute_pressure (bmp581_t *me)
{
    if(!me) return;
    
    if(me->config.pressure_enable != 1)
        me->data.absolute_pressure = 0;
    else
        me->data.absolute_pressure = - me->data.pressure * (me->config.altitude + 16000.0 + 64.0 * me->data.temperature) / (me->config.altitude - 16000.0 - 64.0 * me->data.temperature);
}

uint8_t bmp581_i2c_write_register (bmp581_t *me, bmp581_i2c_funcptr_t i2c_write_funcptr)
{
    if(!me || !i2c_write_funcptr) return 0;
    me->i2c_write = i2c_write_funcptr;
    return 1;
}

uint8_t bmp581_i2c_read_register (bmp581_t *me, bmp581_i2c_funcptr_t i2c_read_funcptr)
{
    if(!me || !i2c_read_funcptr) return 0;
    me->i2c_read = i2c_read_funcptr;
    return 1;
}

uint8_t bmp581_delay_us_register (bmp581_t *me, bmp581_delay_funcptr_t delay_us_funcptr)
{
    if(!me || !delay_us_funcptr) return 0;
    me->delay_us = delay_us_funcptr;
    return 1;
}

uint8_t bmp581_i2c_check (bmp581_t *me)
{
    if(!me) return 0;
    uint8_t _byte;
    return me->i2c_read(me->client_address, &_byte, 1);
}

uint8_t bmp581_set_power_mode (bmp581_t *me, BMP581_POWER_MODE_e power_mode)
{
    if(!me) return 0;
    // TO BE DONE
    return 1;
}

uint8_t bmp581_get_device_id (bmp581_t *me)
{
    if(!me) return 0;
    
    uint8_t dev_id;
    if(!_bmp581_read_regs(me, BMP581_REG_CHIP_ID, &dev_id, 1)) return 0;
    me->data.device_id = dev_id;
    
    return 1;
}

uint8_t bmp581_get_device_status (bmp581_t *me)
{
    if(!me) return 0;
    
    uint8_t _byte;
    if(!_bmp581_read_regs(me, BMP581_REG_STATUS, &_byte, 1)) return 0;
    me->status.int_asserted = (_byte & BMP581_INT_ASSERTED_DRDY) ? 1 : 0;
    
    return 1;
}

void bmp581_get_default_config(bmp581_config_t *config)
{
    if(!config) return;
    
    config->power_mode = BMP581_POWER_MODE_CONTINOUS;
    config->pressure_enable = 1;
    config->iir_filter_temperature   = 1;
    config->iir_filter_pressure      = 1;
    config->iir_shutdown_temperature = 1;
    config->iir_shutdown_pressure    = 1;
    
    config->int_mode     = BMP581_INT_MODE_PULSED;
    config->int_polarity = BMP581_INT_POLARITY_HIGH;
    config->int_drive    = BMP581_INT_DRIVE_PUSHPULL;
    config->int_enable   = BMP581_INT_EN_ENABLE;
    
    config->drdy_enable = 1;
    
    config->altitude = 310; // Czech Republic - Pilsen
}
