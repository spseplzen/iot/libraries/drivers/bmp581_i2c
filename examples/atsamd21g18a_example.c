#include <stddef.h>
#include <stdbool.h>
#include <stdlib.h>
#include "definitions.h"

#include "bmp581/bmp581.h"


void delay_ms(uint32_t ms){
    SYSTICK_TimerStart();
    SYSTICK_DelayMs(ms);
    SYSTICK_TimerStop();
}

void delay_us(uint32_t us){
    SYSTICK_TimerStart();
    SYSTICK_DelayUs(us);
    SYSTICK_TimerStop();
}


uint8_t i2c_write(uint16_t client_address, uint8_t *data, uint8_t size){
    SERCOM1_I2C_Write(client_address, data, size);
    while(SERCOM1_I2C_IsBusy());
    return !(SERCOM1_I2C_ErrorGet());
}

uint8_t i2c_read(uint16_t client_address, uint8_t *data, uint8_t size){
    SERCOM1_I2C_Read(client_address, data, size);
    while(SERCOM1_I2C_IsBusy());
    return !(SERCOM1_I2C_ErrorGet());
}


int main(void)
{
    SYS_Initialize(NULL);
    
    bmp581_t bmp581 = {};
    
    bmp581_i2c_read_register(&bmp581, i2c_read);
    bmp581_i2c_write_register(&bmp581, i2c_write);
    bmp581_delay_us_register(&bmp581, delay_us);
    
    bmp581_config_t config;
    config.power_mode = BMP581_POWER_MODE_CONTINOUS;
    
    config.pressure_enable = 1;
    
    //config.output_data_rate = 0x1C; // not for continous
    
    config.iir_filter_temperature   = 1;
    config.iir_filter_pressure      = 1;
    config.iir_shutdown_temperature = 1;
    config.iir_shutdown_pressure    = 1;
    
    config.int_mode     = 0;
    config.int_polarity = 1;
    config.int_drive    = 0;
    config.int_enable   = 1;
    
    config.drdy_enable = 1;
    
    config.altitude = 310;
    
    if(bmp581_init(&bmp581, BMP581_I2C_CLIENT_ADDRESS_SDO0, &config)){
        printf("INIT\r\n");
    }
    else{
        printf("I2C ERROR\r\n");
    }

    while(true)
    {
        SYS_Tasks();
        
        bmp581_get_device_status(&bmp581);

        if(bmp581.status.int_asserted){
        
            if(!bmp581_get_data(&bmp581) && !bmp581_get_device_id(&bmp581)){
                printf("I2C ERROR\r\n");
            }
            else{
                printf("device id = 0x%02X\r\n", bmp581.data.device_id);
                printf("temperature = %.04f C, pressure = %.04f hPA\r\n", bmp581.data.temperature, bmp581.data.pressure / 100.0);
                bmp581_convert_data_to_absolute_pressure(&bmp581);
                printf("absolute pressure: %.04f hPa\r\n\r\n", bmp581.data.absolute_pressure / 100.0);
            }
            
            delay_ms(500);
            
        }
        
    }

    return (EXIT_FAILURE);
}
